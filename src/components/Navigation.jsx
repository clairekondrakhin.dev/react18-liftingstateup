import "./Navigation.css";

//on passe la props à la fonction navigation
function Navigation({ user }) {
  return (
    <ul className="Navigation">
      <li>
        <img src="https://via.placeholder.com/200x100" alt="Logo" />
      </li>
      <li>
        <h2>Awesome website!</h2>
      </li>
      <div>{user ? <p>{user.name}</p> : <p>Please log in</p>}</div> 
    </ul>
  );
}

//ligne 12 expression ternaire qui affiche "nom de l'utilisateur" si l'utilisateur est connecté, sinon affiche "Please log in"
// ici 'si user est vrai' ou si 'user est faux' est définit dans le composant profil. 

export default Navigation;
