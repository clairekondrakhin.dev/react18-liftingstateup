import { useState } from "react";
import "./App.css";
import Navigation from "./components/Navigation";
import Profile from "./components/Profile";

function App() {
  const [user, setUser] = useState(null);

  return (
    <div className="App">
      <Navigation user={user} />
      <Profile user={user} setUser={setUser} />
    </div>
  );
}

// ligne 11 on passe la prop user au composant navigation pour gérer l'affichage du message si connexion ou non
export default App;
